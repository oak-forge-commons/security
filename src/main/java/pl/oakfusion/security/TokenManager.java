package pl.oakfusion.security;

import java.util.List;

public interface TokenManager {

    String createToken(String emailAddress, List<String> roles);

    String getEmailFromToken(String token);
}
